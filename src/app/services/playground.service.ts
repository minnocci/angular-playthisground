import { Injectable } from '@angular/core';
import { Playground } from '../shared/playground';
import { PLAYGROUNDS } from '../shared/playgrounds';

@Injectable()
export class PlaygroundService {

  constructor() { }

  getPlaygrounds(): Playground[] {
    return PLAYGROUNDS;
  }

  getPlayground(id: number): Playground {
    return PLAYGROUNDS.filter((playground) => (playground.id === id))[0];
  }

}
