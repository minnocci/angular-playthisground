import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Playground } from '../shared/playground';
import { PlaygroundService } from '../services/playground.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})

export class PlaygroundComponent implements OnInit {

  playground: Playground;

  constructor(
    private playgroundService: PlaygroundService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.params['id'];
    this.playground = this.playgroundService.getPlayground(id);
  }

  renderOverallRating(stars: number) {
    return '<span class="fa fa-star"></span>'.repeat(stars);
  }

}
