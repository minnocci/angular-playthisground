import { Component, OnInit } from '@angular/core';
import { Playground } from '../shared/playground';
import { PLAYGROUNDS } from '../shared/playgrounds';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // google maps initial values
  zoom = 15;
  lat = 52.531818;
  lng = 13.422588;

  // doomie hardcoded data for playgrounds
  playgrounds: Playground[] = PLAYGROUNDS;

  constructor() { }

  ngOnInit() {
  }

  renderOverallRating(playground: Playground) {
    return '<span class="fa fa-star"></span>'.repeat(playground.overallRating);
  }

}
