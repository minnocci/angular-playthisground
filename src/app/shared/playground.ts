import { Comment } from './comment';

export class Playground {
  id: number;
  name: string;
  distance: string;
  size: string;
  ages: string;
  equipment: string;
  floorType: string;
  environment: string;
  accessibility: string;
  lastUpdated: string;
  overallRating: number;
  comments: Comment[];
}
