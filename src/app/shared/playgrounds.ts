import { Playground } from './Playground';

export const PLAYGROUNDS: Playground[] = [
  {
    id: 0,
    name: 'Park at A Street',
    distance: '300m',
    size: '300sqm',
    ages: '4-6',
    equipment: 'Pirate ship, several slides',
    floorType: 'Sand',
    environment: 'Quiet and safe',
    accessibility: 'Handicap facilities',
    lastUpdated: '2017-12-13T17:57:28.556094Z',
    overallRating: 5,
    comments: [
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Peter Pan',
        date: '2017-12-17T17:57:28.556094Z'
      },
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Homer Simpson',
        date: '2017-12-19T17:57:28.556094Z'
      }
    ]
  },
  {
    id: 1,
    name: 'Fountain at B Street',
    distance: '700m',
    size: '150sqm',
    ages: '5-7',
    equipment: 'Fountain, several slides',
    floorType: 'Grass',
    environment: 'Some traffic around',
    accessibility: 'No handicap facilities',
    lastUpdated: '2017-12-11T17:57:28.556094Z',
    overallRating: 4,
    comments: [
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Peter Pan',
        date: '2017-12-17T17:57:28.556094Z'
      },
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Homer Simpson',
        date: '2017-12-19T17:57:28.556094Z'
      }
    ]
  },
  {
    id: 2,
    name: 'Playground at C Road',
    distance: '750m',
    size: '200sqm',
    ages: '3-5',
    equipment: 'Pirate ship, several slides',
    floorType: 'Sand, grass',
    environment: 'Quiet and safe',
    accessibility: 'Handicap facilities',
    lastUpdated: '2017-11-13T17:57:28.556094Z',
    overallRating: 4,
    comments: [
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Peter Pan',
        date: '2017-12-17T17:57:28.556094Z'
      },
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Homer Simpson',
        date: '2017-12-19T17:57:28.556094Z'
      }
    ]
  },
  {
    id: 3,
    name: 'Court at D Street',
    distance: '880m',
    size: '400sqm',
    ages: '7-10',
    equipment: 'Football yard, fountain',
    floorType: 'Concrete, sand',
    environment: 'Quiet and safe',
    accessibility: 'Handicap facilities',
    lastUpdated: '2017-10-13T17:57:28.556094Z',
    overallRating: 3,
    comments: [
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Peter Pan',
        date: '2017-12-17T17:57:28.556094Z'
      },
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Homer Simpson',
        date: '2017-12-19T17:57:28.556094Z'
      }
    ]
  },
  {
    id: 4,
    name: 'Park at B Road',
    distance: '1km',
    size: '100sqm',
    ages: '1-4',
    equipment: 'Swings, sandbox',
    floorType: 'Sand',
    environment: 'Busy on weekends',
    accessibility: 'No handicap facilities',
    lastUpdated: '2017-12-10T17:57:28.556094Z',
    overallRating: 2,
    comments: [
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Peter Pan',
        date: '2017-12-17T17:57:28.556094Z'
      },
      {
        comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
        author: 'Homer Simpson',
        date: '2017-12-19T17:57:28.556094Z'
      }
    ]
  }
];
